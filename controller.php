<?php

namespace Concrete\Package\GithubMilestones;

use \Concrete\Core\Package\Package;
use \Concrete\Core\Block\BlockType\BlockType;

class Controller extends Package
{

    protected $pkgHandle = 'github_milestones';
    protected $appVersionRequired = '8.2.0';
    protected $pkgVersion = '1.0';

    public function getPackageDescription()
    {
        return t('Adds a list of github milestones to a page.');
    }

    public function getPackageName()
    {
        return t('GitHub Milestones');
    }

    public function install()
    {
        $pkg = parent::install();

        // install block
        BlockType::installBlockTypeFromPackage('github_milestones', $pkg);
    }

}